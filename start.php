<?php
/**
 * Catch spammers based on behavior
 */

elgg_register_event_handler('init', 'system', 'death_init');

function death_init() {
	elgg_register_plugin_hook_handler('action', 'pages/edit', 'death_check_pages');
	elgg_register_page_handler('spam', 'death_page_handler');
}

/**
 * Display a message to the banned user
 */
function death_page_handler() {
	$title = "We think you are a spammer";

	$content = elgg_view('output/longtext', array(
		'value' => "We think you are a spammer. If you are not, send an email to ". elgg_get_site_entity()->email ." and include your username.",
	));
	
	$body = elgg_view_layout('content', array(
		'filter' => '',
		'content' => $content,
		'title' => $title,
	));

	echo elgg_view_page($title, $body);
}

/**
 * Check a page submission for spam
 */
function death_check_pages() {
	$score = 0;
	$threshold = 100;
	$user = get_loggedin_user();

	$lifetime = (time() - $user->time_created) / (60 * 60);
	if ($lifetime < 6) {
		$score += 80;
	} elseif ($lifetime < 12) {
		$score += 50;
	}

	$text = get_input('description');
	$num_links = substr_count($text, 'href');
	$num_links = min(array($num_links, 5));
	$score += 20 * $num_links;
	
	$num_entities = elgg_get_entities(array(
		'owner_guid' => $user->getGUID(),
		'count' => true,
	));
	if ($num_entities == 0) {
		$score += 20;
	}

	if ($score > $threshold) {
		death_report_spammer($user, $text, $score);
	}
}

/**
 * Handle caught spammers
 *
 * @param ElggUser $user
 * @param string   $text
 * @param int      $score
 */
function death_report_spammer($user, $text, $score) {
	if (is_plugin_enabled('reportedcontent')) {
		$report = new ElggObject;
		$report->subtype = "reported_content";
		$report->owner_guid = $user->getGUID(); // no good way to get admin in Elgg 1.7
		$report->title = "Spammer ban with score of $score";
		$report->address = get_loggedin_user()->getURL();
		$report->description = "Spam text:\n $text";
		$report->access_id = ACCESS_PRIVATE;
		$report->save();
	}

	ban_user($user->getGUID(), "death to spammers ($score)");

	forward(elgg_get_site_url() . 'spam/');
}
